package it.lundstedt.erik.openrpg.lib;

public class Mob
{
String name;

int health;
int baseAtk;
int baseDef;
int totalDef;


public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public int getHealth() {
	return health;
}

public void setHealth(int health) {
	this.health = health;
}

public int getBaseAtk() {
	return baseAtk;
}

public void setBaseAtk(int baseAtk) {
	this.baseAtk = baseAtk;
}

public int getBaseDef() {
	return baseDef;
}

public void setBaseDef(int baseDef) {
	this.baseDef = baseDef;
}

public int getTotalDef() {
	return totalDef;
}

public void setTotalDef(int totalDef) {
	this.totalDef = totalDef;
}


}
