package it.lundstedt.erik.openrpg.game;

import it.lundstedt.erik.menu.Menu;

public class MenuCfg
{
String[] header={"WELCOME TO OpenMenu","version 5.0","its free and","open source","under the don't be a jerk licence"};
String[] menuItems={"first","second","third"};

public MenuCfg(String[] header, String[] menuItems) {
	this.header = header;
	this.menuItems = menuItems;
}

public String[] getHeader() {
	return header;
}

public MenuCfg setHeader(String[] header) {
	this.header = header;
	return this;
}

public String[] getMenuItems() {
	return menuItems;
}

public MenuCfg setMenuItems(String[] menuItems) {
	this.menuItems = menuItems;
	return this;
}



}
